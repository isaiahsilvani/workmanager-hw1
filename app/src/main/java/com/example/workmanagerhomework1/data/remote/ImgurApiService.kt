package com.example.workmanagerhomework1.data.remote

import com.example.workmanagerhomework1.domains.models.UploadResponse
import com.example.workmanagerhomework1.util.Constants
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface ImgurApiService {
    @Multipart
    @POST(Constants.UPLOAD_ENDPOINT)
    suspend fun uploadImage(
        @Header("Authorization") auth: String = "CLIENT-ID ${Constants.CLIENT_ID}",
        @Part image: MultipartBody.Part?
    ): Response<UploadResponse>

    companion object {
        fun getInstance() = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create<ImgurApiService>()
    }

}