package com.example.workmanagerhomework1

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Space
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.example.workmanagerhomework1.ui.theme.WorkManagerHomework1Theme
import com.example.workmanagerhomework1.util.Constants
import com.example.workmanagerhomework1.worker.ImgurWorker
import kotlinx.coroutines.*

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WorkManagerHomework1Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    ShowScreen()
                }
            }
        }
    }
}

@Composable
fun ShowScreen(
    viewModel: MainViewModel = androidx.lifecycle.viewmodel.compose.viewModel()
) {
    // viewModel.workInfoData.observeAsState().value
    val context = LocalContext.current
    var selectImage by remember {
        mutableStateOf<Uri?>(null)
    }
    val launcher = rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri ->
        selectImage = uri
    }
    Scaffold(content = {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Bottom
        ) {
            // Display Image
            DisplayImage(selectImages = selectImage, context = context)
            // Buttons
            Row() {
                Button(onClick = {
                    launcher.launch("image/*")
                }) {
                    Text(text = "Selected a photo")
                }
                Spacer(modifier = Modifier.width(10.dp))
                selectImage?.let {
                    Button(onClick = {
                        viewModel.startWork(context, selectImage!!)
                    }) {
                        Text(text = "Upload photo")
                    }
                }
            }
        }
    })
}

@Composable
fun DisplayImage(
    selectImages: Uri?,
    context: Context
){
    val bitmap = remember {
        mutableStateOf<Bitmap?>(null)
    }
    selectImages?.let {
        if (Build.VERSION.SDK_INT < 28) {
            bitmap.value = MediaStore.Images
                .Media.getBitmap(context.contentResolver, it)
        } else {
            val source = ImageDecoder.createSource(context.contentResolver, it)
            bitmap.value = ImageDecoder.decodeBitmap(source)
        }
        bitmap.value?.let { btm ->
            Image(
                bitmap = btm.asImageBitmap(),
                contentDescription = null,
                modifier = Modifier.size(400.dp)
            )
        }
    }
}

fun jawn() {
    val scope = CoroutineScope(Dispatchers.IO)
    val test = runBlocking {

    }
    val test1 = scope.launch {

    }
    val test3 = scope.async {  }
    val shit = test3
}