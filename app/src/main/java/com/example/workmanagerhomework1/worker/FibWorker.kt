package com.example.workmanagerhomework1.worker

import android.content.Context
import android.util.Log
import androidx.compose.ui.unit.Constraints
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.workmanagerhomework1.util.Constants
import kotlinx.coroutines.delay

class FibWorker(context: Context, workerParameters: WorkerParameters) : CoroutineWorker(context, workerParameters) {
    override suspend fun doWork(): Result {
        val link = inputData.getString(Constants.KEY_IMAGE_URI) ?: ""
        val fib = fib(5)
        Log.e("FibWorker", fib.toString())
        delay(3000L)
        val outputData = workDataOf(Constants.FIB_NUM to fib)
        return Result.success(outputData)
    }

    private fun fib(num: Int): Int {
        if(num <= 2){
            return 1
        }
        return fib(num - 1) + fib(num - 2)
    }
}